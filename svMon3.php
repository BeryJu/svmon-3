<html>
	<head>
		<title>svMon3</title>
	</head>
	<body>
		<header>
			<h1>svMon3</h1>
		</header>
		<div id="main">
			<?php
/**
 * Main Interface class
 */
class svMon3{

	/**
	 * Constructor, shows the Login form
	 */
	function __construct(){
		self::displayForm(false);
	}

	/**
	 * Displays the Form itself, also authenticate
	 * @param  Boolean $previousFail True if Login failed the attempt before
	 * @return Object                Nothing
	 */
	function displayForm($previousFail = false){
		include './Include/DBInterface.php';
		include './Include/CookieInterface.php';
		if (isset($_POST[constant("PREFIX").'user']) &&
			isset($_POST[constant("PREFIX").'password'])){
			if (self::authenticate($_POST[constant("PREFIX").'user'],
									$_POST[constant("PREFIX").'password'],
									$_POST[constant("PREFIX").'save']) == true){
				self::authenticationSuccess();
			}else{
				self::authenticationFailure();
			}
		}else{
			$PREFIX = constant("PREFIX");
			if ($previousFail == false){
				echo "<p id=\"loginf\">Login Failure</p>";
			}
			echo "<form id=\"login\" action=\"svMon3.php\" method=\"POST\">
				<input type=\"text\" name=\"".$PREFIX."user\" value=\"Username\" /><br />
				<input type=\"password\" name=\"".$PREFIX."password\"><br />
				<input type=\"checkbox\" name=\"".$PREFIX."save\"/><br />
				<input type=\"submit\" />
			</form>";
		}
	}

	/**
	 * Called if the authentication was Successful
	 * @return Object Nothing
	 */
	function authenticationSuccess(){
		self::displayInterface();
	}

	/**
	 * Called if the authentication Failed
	 * @return Object Nothing
	 */
	function authenticationFailure(){
		self::displayForm(true);
	}

	/**
	 * Authenticates the User
	 * @param  String $user     The Username
	 * @param  String $password The Password the of the User
	 * @param  Boolean $save     If true, cookie gets set
	 * @return Boolean           Returns true, if Authentication was successful
	 */
	function authenticate($user, $password, $save){
		$DB = new DBInterface();
		$C = new CookieInterface();
		if ($DB->login($user, $password) == true){
			//User is available and logged in
			$_SESSION[constant("PREFIX").'user'] = $user;
			// Do I need dis?
			//$_SESSION[constant("PREFIX").'password'] = $password;
			if ($save){
				$C->save($user, $password);
			}
			return true;
		}else{
			return false;
		}
	}

	function displayInterface(){
		
	}

}

$s = new svMon3();
?>
		</div>
	</body>
</html>