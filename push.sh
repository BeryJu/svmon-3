#!/bin/bash
mv Settings/DBCredentials.php ..
rm -r Documentation
phpdoc -d . -t Documentation
git add .
git commit -m "$1"
git push --all
mv ../DBCredentials.php Settings/DBCredentials.php
