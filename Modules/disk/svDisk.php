<?php
//Classname needs to be the same as the filename
class svDisk extends svModule{

	public function __construct(){}

	public function getPanelName(){
		return "Disk";
	}

	public function getData(){
		if (PHP_OS == "Linux"){
			$path = "/";
		}else{
			$path = "C:";
		}
		$df = disk_free_space($path);
		$dt = disk_total_space($path);
		$du = $dt - $df;
		$df = $this->formatSize($df);
		$du = $this->formatSize($du);
		$dt = $this->formatSize($dt);
		$dp = 100 - sprintf('%.2f', $df / ($dt / 100));

		$data = array('Free' => $df,
						'Size' => $dt,
						'Usage' => $du,
						'Used' => $dp . "%");
		return $data;
	}

	public function formatSize($bytes){
		$types = array( 'B', 'KB', 'MB', 'GB', 'TB' );
		        for( $i = 0; $bytes >= 1024 && $i < ( count( $types ) -1 ); $bytes /= 1024, $i++ );
		                return( round( $bytes, 2 ) . " " . $types[$i] );
	}

}
?>