<?php
//Classname needs to be the same as the filename
class svGeneral extends svModule{

	public $ip;

	public function __construct(){
		$this->ip = $data->ip;
	}

	public function getPanelName(){
		return "General";
	}

	public function getData(){
		$server = gethostbyaddr($_SERVER['SERVER_ADDR']);
		$address = gethostbyaddr($this->ip);
		$intIP = $_SERVER['SERVER_ADDR'];
		$extIP = $this->ip;
		$data = array("Server" => $server, "Address" => $address,
						"Int. IP" => $intIP, "Ext. IP" => $extIP);
		return $data;
	}

}
?>