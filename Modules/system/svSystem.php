<?php
//Classname needs to be the same as the filename
class svSystem extends svModule{

	public function __construct(){
		
	}

	public function getPanelName(){
		return "System";
	}

	public function getData(){
		$uptime = explode("\n", shell_exec('Modules/system/uptime.sh'))[0];
		$load = sys_getloadavg();
		$kernelv = shell_exec("uname -v");
		$fload = $load[0] . ", " . $load[1] . ", " . $load[2];
		$v = $this::getLinuxVersion();
		$data = array("Version" => $v , "Kernel" => $kernelv, "Uptime" => $uptime, "Load" => $fload);
		return $data;
	}

	public function getLinuxVersion(){
		$cont = shell_exec("cat /etc/os-release | grep PRETTY_NAME");
		$cont = explode("=", $cont)[1];
		$cont = str_replace("\"", "", $cont);
		return $cont;
	}

}
?>