<?php
/**
 * A Modules to show the recent Apache2 Activity
 * 
 * @package svMon3_Modules
 */
class svApache2 extends svModule{

	public $lines = 10;

	function __construct(){

	}

	/**
	 * Returns the Modules name
	 * @return String Module name
	 */
	public function getPanelName(){
		return "Apache2";
	}

	public function getData(){
		$data = explode("\n", shell_exec("tail -n " . $this->lines . " /var/log/apache2/access.log"));
		$final = array();
		foreach ($data as &$value) {
			if (!($value == "")){
				$cont = explode("?", explode("\"" , explode("[", $value)[1])[1])[0];
				$cont = self::shortenText($cont);
				$final[explode(" - - ", $value)[0].str_repeat(" ", $index)] = $cont;
				$index ++;
			}
		}
		return $final;
	}

	 function shortenText($text) { 
		$chars = 75; 
		if (strlen($text) >= $chars){
			$text = $text." "; 
			$text = substr($text,0,$chars); 
			$text = substr($text,0,strrpos($text,' ')); 
			$text = $text."..."; 
		}
		return $text; 
    }

}
?>