<?php
//Classname needs to be the same as the filename
class svDiagnostics extends svModule{


	function __construct(){}

	public function getPanelName(){
		return "Diagnostics";
	}

	public function getData(){
		$final = array('Allocated RAM' => self::formatSize(memory_get_usage()));
		return $final;
	}

	public function formatSize($bytes){
		$types = array( 'B', 'KB', 'MB', 'GB', 'TB' );
		        for( $i = 0; $bytes >= 1024 && $i < ( count( $types ) -1 ); $bytes /= 1024, $i++ );
		                return( round( $bytes, 2 ) . " " . $types[$i] );
	}

}
?>