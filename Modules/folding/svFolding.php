<?php
//Classname needs to be the same as the filename
class svFolding extends svModule{

	function __construct(){
		
	}

	public function getPanelName(){
		return "Folding";
	}

	public function getData(){
		$data = explode("\n", shell_exec("tail -n " . $this->lines . " /var/log/apache2/access.log"));
		$final = array();
		$index = 1;
		foreach ($data as &$value) {
			if (!($value == "")){
				//Is it the same IP as me?
				//Spec or Admin?
				$cont = explode("?", explode("\"" , explode("[", $value)[1])[1])[0];
				$cont = self::shortenText($cont);
				$final[$index . ": " . explode(" - - ", $value)[0]] = $cont;
				$index++;
			}
		}
		return $final;
	}

	 function shortenText($text) { 
		$chars = 75; 
		if (strlen($text) >= $chars){
			$text = $text." "; 
			$text = substr($text,0,$chars); 
			$text = substr($text,0,strrpos($text,' ')); 
			$text = $text."..."; 
		}
		return $text; 
    }

}
?>