<?php
class svFans extends svModule{

	public function __construct(){
		
	}

	public function getPanelName(){
		return "Fans";
	}

	public function getData(){
		return self::getFans();
	}

	public function getFans(){
		$arr = explode("\n", shell_exec("sensors"));
		$res = array();
		foreach ($arr as &$value) {
			//Only if its not a fan nor voltage
	 		if (!(self::startsWith($value, "temp") or self::startsWith($value, "in") or self::startsWith($value, "cpu"))){
	  		  	//it's a temperature
	  		  	if (self::startsWith($value, "fan")){
	  		  		$degrees = explode(':', explode('(', $value)[0])[0];
	  		  		$tempc = explode(':', explode('(', $value)[0])[1];
	  		  		$res[$degrees] = $tempc;
	  		  	}
	 		}
		}
		return $res;
	}

	function startsWith($haystack, $needle){
	    return !strncmp($haystack, $needle, strlen($needle));
	}

}
?>