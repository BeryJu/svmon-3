<?php
class svVoltages extends svModule{

	public function __construct(){
		
	}
	
	public function getPanelName(){
		return "Voltages";
	}

	public function getData(){
		return self::getVoltages();
	}

	public function getVoltages(){
		$arr = explode("\n", shell_exec("sensors"));
		$res = array();
		foreach ($arr as &$value) {
			//Only if its not a fan nor voltage
	 		if (!(self::startsWith($value, "fan") or self::startsWith($value, "temp"))){
	  		  	//it's a temperature
	  		  	if (self::startsWith($value, "in") or self::startsWith($value, "cpu")){
	  		  		$degrees = explode(':', explode('(', $value)[0])[0];
	  		  		$tempc = explode(':', explode('(', $value)[0])[1];
	  		  		$res[$degrees] = $tempc;
	  		  	}
	 		}
		}
		return $res;
	}

	function startsWith($haystack, $needle){
	    return !strncmp($haystack, $needle, strlen($needle));
	}

}
?>