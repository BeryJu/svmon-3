<?php
class svTemperatures extends svModule{

	public function __construct(){
		
	}
	
	public function getPanelName(){
		return "Temperatures";
	}

	public function getData(){
		return self::getTemperatures();
	}

	public function getTemperatures(){
		$arr = explode("\n", shell_exec("sensors"));
		$res = array();
		foreach ($arr as &$value) {
			//Only if its not a fan nor voltage
	 		if (!(self::startsWith($value, "fan") or self::startsWith($value, "in") or self::startsWith($value, "cpu"))){
	  		  	//it's a temperature
	  		  	if (self::startsWith($value, "temp")){
	  		  		$degrees = explode(':', explode('(', $value)[0])[0];
	  		  		$tempc = explode(':', explode('(', $value)[0])[1];
	  		  		$res[ucfirst($degrees)] = $tempc;
	  		  	}elseif (self::startsWith($value, "Core")) {
	  		  		$degrees = explode(':', explode('(', $value)[0])[0];
	  		  		$tempc = explode(':', explode('(', $value)[0])[1];
	  		  		$res[$degrees] = $tempc;
	  		  	}
	 		}
		}
		return $res;
	}

	function startsWith($haystack, $needle){
	    return !strncmp($haystack, $needle, strlen($needle));
	}

}
?>