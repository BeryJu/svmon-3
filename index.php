<?php
/**
 * The main file
 *
 * @package svMon3
 */
session_start();
require_once 'Include/File.php';
require_once 'Include/DBInterface.php';
require_once 'Include/UserDBInterface.php';
require_once 'Include/CookieInterface.php';
require_once 'Include/Browser.php';
require_once 'Include/String.php';
/**
 * The main file
 *
 * @package svMon3
 */
class svMon3{

	/**
	 * Logging to the file Debug/log.txt
	 * @var File
	 */
	var $logger;

  var $browser;

	/**
	 * Constructor, shows the Login form
	 */
	function __construct(){
    $this->browser = new Browser();
		//Logging stuff
    $this->logger = File::path("Debug/log.txt");
		$db = new UserDBInterface(true);
		$dbe = $db->checkIfDBExists();
		if ($dbe){
			//Logging stuff
      $this->log("DB not existing");
			if (isset($_POST['user']) && isset($_POST['password'])){
				//Logging stuff
        $this->log("Creating DB");
				$db->createDB();
				$db->addUser($_POST['user'], $_POST['password']);
				//Logging stuff
        $this->log ("Showing final");
				self::displayForm();
			}
			//Logging stuff
      $this->log("Show Setup");
			self::displayInitialSetup();
			die();
		}
		//Logging stuff
    $this->log ("Showing final");
		self::displayForm();
	}

	/**
	 * Write something to the Log file
	 * @param  String $content The content to write
	 * @return Object          Nothing
	 */
	function log($content){
		//Logging stuff
    $this->logger->write("[".time()."]".$content);
	}

	/**
	 * Displays the initial setup
	 * @return Object Nothing
	 */
	function displayInitialSetup(){
		echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <title>Initial Setup &middot; svMon3</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <!-- Le styles -->
    <link href=\"Site/Bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
    <style type=\"text/css\">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type=\"text\"],
      .form-signin input[type=\"password\"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href=\"Site/Bootstrap/css/bootstrap-responsive.css\" rel=\"stylesheet\">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"Site/Bootstrap/ico/apple-touch-icon-144-precomposed.png\">
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"Site/Bootstrap/ico/apple-touch-icon-114-precomposed.png\">
      <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"Site/Bootstrap/ico/apple-touch-icon-72-precomposed.png\">
                    <link rel=\"apple-touch-icon-precomposed\" href=\"Site/Bootstrap/ico/apple-touch-icon-57-precomposed.png\">
                                   <link rel=\"shortcut icon\" href=\"Site/Bootstrap/ico/favicon.png\">
  </head>

  <body>

    <div class=\"container\">
      <h1>svMon3 - Initial Setup</h1>
      <form class=\"form-signin\" action=\"index.php\" method=\"POST\">
        <h2 class=\"form-signin-heading\">Main User Credentials</h2>
        <input name=\"user\" type=\"text\" class=\"input-block-level\" placeholder=\"Email address\">
        <input name=\"password\" type=\"password\" class=\"input-block-level\" placeholder=\"Password\">
        <button class=\"btn btn-large btn-primary\" type=\"submit\">Sign in</button>
      </form>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"Site/Bootstrap/js/jquery.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-transition.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-alert.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-modal.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-dropdown.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-scrollspy.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tab.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tooltip.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-popover.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-button.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-collapse.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-carousel.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-typeahead.js\"></script>

  </body>
</html>";
		die();
	}

	/**
	 * Displays the Form itself, also authenticate
	 * @param  Boolean $previousFail True if Login failed the attempt before
	 * @return Object                Nothing
	 */
	function displayForm($previousFail = false){
		$cookie = new CookieInterface();
		if (isset($_POST[constant("PREFIX").'user']) &&
			isset($_POST[constant("PREFIX").'password'])){
			//Logging stuff// 
      $this->log("POST data available");
			//Formular was used, therfore POST data
			if (self::authenticate($_POST[constant("PREFIX").'user'],
				$_POST[constant("PREFIX").'password'],
				$_POST[constant("PREFIX").'save']) == true){
				//Logging stuff// 
        $this->log("POST data login succeded. Welcome back, ".$_POST[constant("PREFIX").'user']);
				self::authenticationSuccess($_POST[constant("PREFIX").'user']);
			}else{
				//Logging stuff// 
        $this->log("POST data login failed");
				self::authenticationFailure();
			}
		}elseif ($cookie::checkForLogin()) {
			//Cookies were found, therfore COOKIE
			//Logging stuff// 
      $this->log("COOKIE data available");
			if (self::authenticate($_COOKIE[constant("PREFIX").'user'],
				$_COOKIE[constant("PREFIX").'password'],
				$_COOKIE[constant("PREFIX").'save']) == true){
				//Logging stuff// 
        $this->log("COOKIE login succeded. Welcome back, ".$_COOKIE[constant("PREFIX").'user']);
				self::authenticationSuccess($_COOKIE[constant("PREFIX").'user']);
			}else{
				//Logging stuff//
        $this->log("COOKIE login failed");
				$cookie->delete();
				header("Location: index.php");
				die();
			}
		}else{
			//Logging stuff// 
      $this->log("No Logindata available");
			$PREFIX = constant("PREFIX");
			if ($previousFail == true){
				echo "<p id=\"loginf\">Login Failure</p>";
			}
			echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <title>Sign in &middot; svMon3</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <!-- Le styles -->
    <link href=\"Site/Bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
    <style type=\"text/css\">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type=\"text\"],
      .form-signin input[type=\"password\"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href=\"Site/Bootstrap/css/bootstrap-responsive.css\" rel=\"stylesheet\">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"Site/Bootstrap/ico/apple-touch-icon-144-precomposed.png\">
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"Site/Bootstrap/ico/apple-touch-icon-114-precomposed.png\">
      <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"Site/Bootstrap/ico/apple-touch-icon-72-precomposed.png\">
                    <link rel=\"apple-touch-icon-precomposed\" href=\"Site/Bootstrap/ico/apple-touch-icon-57-precomposed.png\">
                                   <link rel=\"shortcut icon\" href=\"Site/Bootstrap/ico/favicon.png\">
  </head>

  <body>

    <div class=\"container\">
      <form action=\"index.php\" class=\"form-signin\" method=\"POST\">
        <h2 class=\"form-signin-heading\">Please sign in</h2>
        <input name=\"".$PREFIX."user\" type=\"text\" class=\"input-block-level\" placeholder=\"Username\">
        <input name=\"".$PREFIX."password\" type=\"password\" class=\"input-block-level\" placeholder=\"Password\">
        <label class=\"checkbox\">
          <input name=\"".$PREFIX."save\" type=\"checkbox\" value=\"remember-me\"> Remember me
        </label>
        <button class=\"btn btn-large btn-primary\" type=\"submit\">Sign in</button>
      </form>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"Site/Bootstrap/js/jquery.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-transition.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-alert.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-modal.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-dropdown.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-scrollspy.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tab.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tooltip.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-popover.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-button.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-collapse.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-carousel.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-typeahead.js\"></script>

  </body>
</html>";
		}
	}

	/**
	 * Called if the authentication was Successful
	 * @return Object Nothing
	 */
	function authenticationSuccess($user){
    if ($this->browser->isMobile()){
  		self::displayDesktopInterface($user);
    }else{
      self::displayMobileInterface();
    }
	}
	/**
	 * Called if the authentication Failed
	 * @return Object Nothing
	 */
	function authenticationFailure(){
		self::displayForm(true);
	}

	/**
	 * Authenticates the User
	 * @param  String $user     The Username
	 * @param  String $password The Password the of the User
	 * @param  Boolean $save     If true, cookie gets set
	 * @return Boolean           Returns true, if Authentication was successful
	 */
	function authenticate($user, $password, $save){
		$DB = new UserDBInterface();
		$C = new CookieInterface();
		if ($DB->login($user, $password) == true){
			//User is available and logged in
			$_SESSION[constant("PREFIX").'user'] = $user;
			// Do I need dis?
			//$_SESSION[constant("PREFIX").'password'] = $password;
			if ($save){
				$C->save($user, $password);
			}
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Displays the main interface
	 * @return Object Nothing
	 */
	function displayDesktopInterface($user){
		echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <title>svMon3</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <!-- Le styles -->
    <link href=\"Site/Bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
        <script type=\"text/javascript\" src=\"Site/Script.js\"></script>
    <link href=\"Site/Data.css\" rel=\"stylesheet\">
    <link rel=\"icon\" type=\"image/png\" href=\"Site/favicon.png\">
    <script type='text/javascript'>
      function init(){
        doRequest('QueryModules.php', 'Main');
        setInterval(function(){doRequest('QueryModules.php', 'Main')}, ".constant("INTERVAL")."); 
      }
      function logout(){
        deleteCookie(\"".constant("PREFIX")."user\");
        deleteCookie(\"".constant("PREFIX")."password\");
        document.location.reload(true);
      }
    </script>
    <link href=\"Site/Bootstrap/css/bootstrap-responsive.css\" rel=\"stylesheet\">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"Site/Bootstrap/ico/apple-touch-icon-144-precomposed.png\">
    <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"Site/Bootstrap/ico/apple-touch-icon-114-precomposed.png\">
      <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"Site/Bootstrap/ico/apple-touch-icon-72-precomposed.png\">
                    <link rel=\"apple-touch-icon-precomposed\" href=\"Site/Bootstrap/ico/apple-touch-icon-57-precomposed.png\">
                                   <link rel=\"shortcut icon\" href=\"Site/Bootstrap/ico/favicon.png\">
  </head>

  <body onload=\"init();\">

    <div class=\"navbar navbar-inverse navbar-fixed-top\">
      <div class=\"navbar-inner\">
        <div class=\"container\">
          <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </a>
          <a class=\"brand\" href=\"#\">svMon3</a>
          <div class=\"nav-collapse collapse\">
            <p class=\"navbar-text pull-right\">
              Logged in as $user
            </p>
            <ul class=\"nav\">
              <li class=\"active\"><a href=\"#\">Home</a></li>
              <li><a onclick=\"Javascript:controlPanel();\" href=\"#\">Control Panel</a></li>
              <li><a onclick=\"Javascript:logout();\" href=\"#\">Logout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class=\"container\">

      <div id=\"Main\">
      </div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"Site/Bootstrap/js/jquery.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-transition.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-alert.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-modal.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-dropdown.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-scrollspy.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tab.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-tooltip.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-popover.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-button.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-collapse.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-carousel.js\"></script>
    <script src=\"Site/Bootstrap/js/bootstrap-typeahead.js\"></script>

  </body>
</html>
";
	}

  function displayMobileInterface(){
    echo "<!DOCTYPE html>
<html>
<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
   <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>
   <link rel=\"stylesheet\" href=\"Site/iUI/iui.css\" type=\"text/css\" />
   <link rel=\"stylesheet\" href=\"Site/iUI/t/default/default-theme.css\" type=\"text/css\"/>
   <script type=\"application/x-javascript\" src=\"Site/iUI/iui.js\"></script>
   <script type=\"text/javascript\" src=\"Site/Script.js\"></script>
    <link href=\"Site/Data.css\" rel=\"stylesheet\">
    <link rel=\"icon\" type=\"image/png\" href=\"Site/favicon.png\">
    <script type='text/javascript'>
      function init(){
        doRequest('QueryModules.php', 'Main');
        setInterval(function(){doRequest('QueryModules.php', 'Main')}, ".constant("INTERVAL")."); 
      }
      function logout(){
        deleteCookie(\"".constant("PREFIX")."user\");
        deleteCookie(\"".constant("PREFIX")."password\");
        document.location.reload(true);
      }
    </script>
    <title>svMon3</title>
</head>
<body onload=\"init();\">
   <div class=\"toolbar\">
      <h1 id=\"pageTitle\">svMon3</h1>
      <a id=\"backButton\" class=\"button\" href=\"#\"></a>
   </div>
   <ul id=\"Main\">
      
   </ul>
</body>
<html>";
  }

}

$s = new svMon3();
?>