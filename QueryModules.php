<?php
/**
 * Abstract Module Class to be extended by Modules
 *
 * @package svMon3
 */
session_start();
require_once 'Settings/Settings.php';
require_once 'Include/String.php';
require_once 'Include/DBInterface.php';
require_once 'Include/UserDBInterface.php';
require_once 'Include/File.php';
require_once 'Include/Browser.php';
/**
 * Abstract Module Class to be extended by Modules
 *
 * @package svMon3
 */
abstract class svModule{

	/**
	 * Constructor
	 */
	abstract public function __construct();
	/**
	 * Returns the Name of the Module's Panel
	 * @return String Panel Name
	 */
	abstract public function getPanelName();
	/**
	 * Used to recive the Data of the Module
	 * @return String|Array         The data
	 */
	abstract public function getData();

}
/**
 * Class to show all the modules
 *
 * @package svMon3
 */
class QueryModules{

	/**
	 * Interface to the user DB
	 * @var DBInterface
	 */
	var $DB;

	var $bro;

	/**
	 * Constructor
	 */
	function __construct(){
		self::importClasses();
		$this->DB = new UserDBInterface();
		$this->bro  = new Browser();
		//if desktop
		if ($this->bro->isMobile()){
			self::renderDesktop(self::createModuleInstances());
		}else{
			self::renderMobile(self::createModuleInstances());
		}
	}

	/**
	 * Iterates over all Modules and Imports them
	 * @param  String $path The path to the Modules
	 * @return Object       Nothing
	 */
	function importClasses($path = "Modules"){
		$ignore = array('.', '..');
		$handle = opendir($path);
		while(false !== ($file = readdir($handle))){    
			if(!in_array($file, $ignore)){  
				if(is_dir("$path/$file")){ 
					self::importClasses("$path/$file");  
				}else{
					if (stringContains($file, "sv") && stringContains($file, ".php")){
						$cpath = "$path/$file";
						include $cpath;
					}
				}
			} 
		}
		closedir($handle);
	}

	/**
	 * Creates a list of Instances of modules
	 * @return Array Instances of Modules
	 */
	function createModuleInstances(){
		$modules = constant('MODULES');
		$modules = explode(',', $modules);
		$mInstances = array();
		foreach ($modules as $key => $value) {
			try {
				$mInstances[] = new $value(null);
			} catch (Exception $e) {
				echo "Please rewrite the Settings/Settings.php<br>$e";
			}
		}
		return $mInstances;		
	}

	/**
	 * Renders the Modules for a Desktop browser
	 * @param  Array $mInstances Instances of the Modules to render
	 * @return Object             Nothing
	 */
	function renderDesktop($mInstances){
		if (isset($_SESSION[constant("PREFIX").'user'])){
			if ($this->DB->existsUser($_SESSION[constant("PREFIX").'user'] )){
				if ($mInstances != null){
					foreach ($mInstances as $key => $value) {
						$name = $value->getPanelName();
						$data = $value->getData($this->user, $this->u_stat);
						echo ("<div class='data bs-docs-example'><h2>$name</h2>");
						foreach ($data as $key => $value){
							self::renderDesktopData($key, $value);
						}
						echo ("</div>");
					}
				}
			}
		}else{
			echo "No user logged in!";
		}
	}

	/**
	 * Renders the data for a Desktop browser
	 * @param  String $key   The Key or Name
	 * @param  String $value The Value (String or Array)
	 * @return Object        Nothing
	 */
	private function renderDesktopData($key, $value){
		if (is_array($value)){
			self::renderDesktopData($key, $value);
		}else{
			echo("<span class=' name'>$key</span>\n");
			echo("<span class=' value'>$value</span><br />\n");
		}
	}

	/**
	 * Renders the Modules for a Mobile browser
	 * @param  Array $mInstances Instances of the Modules to render
	 * @return Object             Nothing
	 */
	function renderMobile($mInstances){
		if (isset($_SESSION[constant("PREFIX").'user'])){
			if ($this->DB->checkIfUserExists($_SESSION[constant("PREFIX").'user'] )){
				if ($mInstances != null){
					foreach ($mInstances as $key => $value) {
						$name = $value->getPanelName();
						$data = $value->getData($this->user, $this->u_stat);
						echo ("<li class=\"group\">$name</li>");
						foreach ($data as $key => $value){
							self::renderMobileData($key, $value);
						}
					}
				}
			}
		}else{
			echo "No user logged in!";
		}
	}

	/**
	 * Renders the data for a Mobile browser
	 * @param  String $key   The Key or Name
	 * @param  String $value The Value (String or Array)
	 * @return Object        Nothing
	 */
	private function renderMobileData($key, $value){
		if (is_array($value)){
			self::renderMobileData($key, $value);
		}else{
			echo("<li>$key: $value</span>\n");
		}
	}

}

$q = new QueryModules();