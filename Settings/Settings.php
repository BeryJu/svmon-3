<?php
/**
 * Important Settings
 *
 * @package svMon3_Settings
 */
/**
 * The Prefix for cookies, post and some other stuff
 */
define('PREFIX', 'svMon3_');
/**
 * Time for Cookies, added to the current time
 * Also the Time that is added when a cookie is refreshed
 */
define('TIME_DELTA', '3600*24*100');
/**
 * Modules to Display
 * Seperatre with a ,
 */
define('MODULES', 'svSystem,svApache2,svTemperatures,svDisk,svDiagnostics');
/**
 * The Interval to refresh the module Data
 */
define('INTERVAL', '1500');
/**
 * The current theme selected
 * 'Default' is the standard one.
 */
define('THEME', 'Default');