<?php
include '../Settings/Settings.php';
include '../Settings/DBCredentials.php';
class DBInterface{

	private $log;

	function __construct(){
		include 'File.php';
		$this->log = File::path("../Debug/log.txt");
		self::connect();
		mysql_select_db(constant('SQLDB')) or self::createDB();
		self::disconnect();
	}

	function createDB(){
		self::connect();
		mysql_query("CREATE DATABASE ".constant('SQLDB'));
		mysql_select_db(constant('SQLDB')) or self::createDB();
		//Users, for login
		$SQLquery = "CREATE TABLE Users (
			id int(6) NOT NULL auto_increment,
			username varchar(20) NOT NULL,
			password varchar(128) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE id (id),
			KEY id_2 (id))";
		mysql_query($SQLquery);
		self::disconnect();
	}

	function connect(){
		mysql_connect(constant('SQLhost'), constant('SQLuser'), constant('SQLpassword'));
	}

	function disconnect(){
		mysql_close();
	}

	function addUser($user, $password){
		if (isset($user) && isset($password)){
			$data = array();
			$data['username'] = $user;
			$data['password'] = self::sha512($password);
			$ex = self::checkIfExists($user);
			if ($ex == false){
				$this->log->write("Creating User ".$user);
				self::connect();
				mysql_select_db(constant('SQLDB')) or self::createDB();
				$SQLquery = "INSERT INTO Users VALUES 
					('',
					'".$data['username']."',
					'".$data['password']."')";
				$SQLresult = mysql_query($SQLquery);
				self::disconnect();
				return true;
			}else{
				return false;
			}
		}
	}

	function login($user, $password){
		if (isset($user) && isset($password)){
			self::connect();
			mysql_select_db(constant('SQLDB')) or self::createDB();
			$SQLquery = "SELECT * FROM Users";
			$SQLresult = mysql_query($SQLquery);
			$SQLrows = mysql_numrows($SQLresult);
			self::disconnect();

			$data = array();
			$data['username'] = $user;
			$data['password'] = self::sha512($password);

			$SQLcurr = 0;
			while ($SQLcurr < $SQLrows) {
				//Login can be Email of Username
				$u = mysql_result($SQLresult, $SQLcurr, "username");
				$p = mysql_result($SQLresult, $SQLcurr, "password");
				if ($data['username'] == $u && $data['password'] == $p){
					return true;
				}
				$SQLcurr++;
			}
			return false;
		}
	}

/*Utils section */

	function sha512($str) {
		for ($x=0; $x < 10; $x++) {
			$str = hash('sha512', $str.gethostname());
		}
		return $str;
	}

	function checkIfExists($username){
		self::connect();
		mysql_select_db(constant('SQLDB')) or self::createDB();
		$SQLquery = "SELECT * FROM Users";
		$SQLresult = mysql_query($SQLquery);
		$SQLrows = mysql_numrows($SQLresult);
		self::disconnect();
		$SQLcurr = 0;
		while ($SQLcurr < $SQLrows) {
			$u = mysql_result($SQLresult, $SQLcurr, "username");
			if ($username == $u){
				$this->log->write("User $username already exists");
				return true;
			}
			$SQLcurr++;
		}
		return false;
	}

}

// class Test{

// 	function __construct(){
// 		$d = new DBInterface();
// 		var_dump($d->login("BeryJu", "derp"));
// 		var_dump($d->login("BeryJu", "herp"));
// 	}

// }
// $t = new Test();