<?php
require_once 'Include/DBInterface.php';
require_once 'Include/String.php';
require_once 'Settings/DBCredentials.php';
class ContentDBInterface extends DBInterface{

	function __construct(){
		if ($this->checkIfDBExists()){
			parent::createDB();
		}
		if ($this->checkIfTableExists("Content")){
			parent::createTable("Content", "title varchar(20) NOT NULL,
				content varchar(5000) NOT NULL");
		}
	}

	function addContent($title, $content){
		$data = array();
		$data['title'] = $title;
		$data['content'] = $content;
		if (parent::checkForEntry("Content", "title = '$title'")){
			return false;
		}else{
			return parent::addEntry($data, "Content");
		}
	}

	function getContent($title){
		$data = array();
		$data['title'] = $title;
		//$data['content'] = "";
		return parent::getEntry($data, "Content", "content","title = '$title'");
	}

}

//Test
$DB = new ContentDBInterface();
$DB->addContent("herp","dsrk0pgtkjweropyjrtopejghiowehjgrtiowhjorihjriohjrio0erp");
echo $DB->getContent("herp");