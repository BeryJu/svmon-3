<?php
require_once 'Include/ContentInterface.php';
abstract class Page{

	abstract function display($contentInterface);

}

class SiteManager{

	var $contentInterface;

	function __construct(){
		$this->contentInterface = new ContentInterface();
	}

	function load($name){
		if (file_exists("Site/$name.php")){
			require_once "Site/$name.php";
			$inst = new $name();
			if (is_subclass_of($inst, "Page")){
				$inst->display($this->contentInterface);
			}else{
				echo "Class $name not extending Abstract Page class.";
			}
		}else{
			echo "File Site/$name.php not existing.";
		}
	}

	function generateJS(){

	}

}

class JavaScriptGenerator{

	function generate($list){
		if (is_array($list)){
			$result = "";
			foreach ($list as $key => $value) {
				$result .= "function $value(){
	window.navigate(\"Site/$value.php\");
}\n";
			}
			return $result;
		}else{
			return false;
		}
	}

}