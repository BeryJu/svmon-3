<?php
include 'String.php';
class Directory{

	private $handle;

	function path($path){
		$instance = new self();
		$instance->open($path);
		return $instance;
	}

	function open($path){
		$this->handle = opendir($path);
	}

	function listFiles(){
		$files = array();
		while (false !== ($file = readdir($this->handle))) {
			if (stringContains($file, ".") == true && $file != "."){
				$files[] = $file;
			}
		}
		return $files;
	}

	function listDir(){
		$dir = array();
		while (false !== ($file = readdir($this->handle))) {
			if (stringContains($file, ".") != true && $file != "."){
				$dir[] = $file;
			}
		}
		return $dir;
	}

	function close(){
		closedir($this->handle);
	}
}
?>