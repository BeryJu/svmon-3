<?php
/**
 * Class used to interface with cookies
 *
 * @package svMon3_Interface
 */
require_once 'Settings/Settings.php';
require_once 'Include/String.php';
/**
 * Class used to interface with cookies
 *
 * @package svMon3_Interface
 */
class CookieInterface{

	/**
	 * Saves a (login) cookie
	 * @param  String $user     The User to save
	 * @param  String $password The Password to save
	 * @return Object           Nothing
	 */
	function save($user, $password){
		setcookie(constant("PREFIX").'user', $user, time() + constant("TIME_DELTA"));
		setcookie(constant("PREFIX").'password', sha512($password), time() + constant("TIME_DELTA"));
	}

	/**
	 * Checks if (login) cookies have been saved
	 * @return Object Nothing
	 */
	function checkForLogin(){
		if (isset($_COOKIE[constant("PREFIX").'user']) && 
			isset($_COOKIE[constant("PREFIX").'password'])){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Refreshes the Time login cookies last
	 * @return Object Nothing
	 */
	function refreshTime(){
		$user = $_COOKIE[constant("PREFIX").'user'];
		$password = $_COOKIE[constant("PREFIX").'password'];
		setcookie(constant("PREFIX").'user', $user, time() + constant("TIME_DELTA"));
		setcookie(constant("PREFIX").'password', $password, time() + constant("TIME_DELTA"));
	}

	function delete(){
		setcookie(constant("PREFIX").'user',"", time() - 3600 , constant("PREFIX"));
		setcookie(constant("PREFIX").'password',"", time() - 3600 , constant("PREFIX"));
	}

}