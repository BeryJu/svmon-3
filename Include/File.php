<?php
/**
 * Class to interface with File in an OOP Way
 *
 * @package svMon3_Utility
 */
/**
 * Class to interface with File in an OOP Way
 *
 * @package svMon3_Utility
 */
class File{

	/**
	 * Handle of the File
	 * @var PHP-File Handle
	 */
	private $handle;
	/**
	 * Path of the File
	 * @var String
	 */
	private $path;

	/**
	 * A Constructor to directly open a file
	 * @param  String $path The path to the file to open
	 * @return File       The instance, with the file opend
	 */
	function path($path){
		$instance = new self();
		$instance->open($path);
		return $instance;
	}

	/**
	 * Opens a file
	 * @param  String $path The path to the file
	 * @return Object       Nothing
	 */
	function open($path){
		$this->path = $path;
		try {
			$this->handle = fopen($path, 'a+');
		} catch (Exception $e) {
			$e->getMessage();
		}
	}

	/**
	 * Reads some content of the file
	 * @param  Int $length The Length to read
	 * @return String         The Content read
	*/
	function read($length){
		return fread($this->handle, $length);
	}

	/**
	 * Reads the whole file
	 * @return String The content of the file
	 */
	function readAll(){
		return fread($this->handle, filesize($this->path));
	}

	/**
	 * Reads the whole File and spilts it in an array of lines
	 * @return Array The lines of the content
	 */
	function readAllLines(){
		return explode("\n", fread($this->handle, filesize($this->path)));
	}

	/**
	 * Writes the content to the file
	 * @param  String $string The content to write
	 * @return Int         The result
	 */
	function write($string){
		return fwrite($this->handle, $string."\n");
	}

	/**
	 * Returns the Size of the file
	 * @return Int Size of the file
	 */
	function getSize(){
		return filesize($this->path);
	}

}
?>