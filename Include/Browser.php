<?php
require_once 'String.php';
class Browser{
	
	var $uagent;

	function __construct(){
		if (isset($_SERVER['HTTP_USER_AGENT'])){
			$this->uagent = $_SERVER['HTTP_USER_AGENT'];
		}else{
			echo "Not used via a browser.\n";
		}
	}

	function isMobile(){
		if (stringContains($this->uagent, "iPhone") ||
			stringContains($this->uagent, "Android")){
			return false;
		}else{
			return true;
		}
	}

}