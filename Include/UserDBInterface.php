<?php
require_once 'Include/DBInterface.php';
require_once 'Include/String.php';
require_once 'Settings/DBCredentials.php';
class UserDBInterface extends DBInterface{

	function createDB(){
		parent::createDB();
		parent::createTable("Users", "username varchar(20) NOT NULL,
			password varchar(128) NOT NULL");
	}

	function addUser($username, $password){
		$data = array();
		$data['username'] = $username;
		$data['password'] = sha512($password);
		if (parent::checkForEntry("Users", "username = '$username'")){
			return false;
		}else{
			return parent::addEntry($data, "Users");
		}
	}

	function login($username, $password){
		$data = array();
		$data['username'] = $username;
		$data['password'] = sha512($password);
		return parent::getEntry($data, "Users", "password","username = '$username'") == $data['password'];
	}

	function existsUser($username){
		$data = array();
		$data['username'] = $username;
		return parent::getEntry($data, "Users", "username", "username = '$username'") == $data['username'];
	}

}