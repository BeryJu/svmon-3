<?php
/**
 * Class to Interface with the Login Database
 *
 * @package svMon3_DB
 */
require_once 'Settings/Settings.php';
require_once 'Include/File.php';
/**
 * Class to Interface with the Login Database
 *
 * @package svMon3_DB
 */
class DBInterface{

	/**
	 * The Log file to write debugging stuff into
	 * @var File
	 */
	private $log;

	/**
	 * Constructor
	 *
	 * @return Object Nothing
	 */
	function __construct(){
		$this->log = File::path("Debug/log.txt");
	}

	/**
	 * Creates the main Database
	 * @return Object Nothing
	 */
	function createDB(){
		self::connect();
		mysql_query("CREATE DATABASE ".constant('SQLDB'));
		mysql_select_db(constant('SQLDB')) or die("Unable to create DB");
		self::disconnect();
	}

	function createTable($table, $dbSig){
		self::connect();
		mysql_select_db(constant('SQLDB'));
		//Users, for login
		$SQLquery = "CREATE TABLE $table (
			id int(6) NOT NULL auto_increment,
			$dbSig,
			PRIMARY KEY (id),
			UNIQUE id (id),
			KEY id_2 (id))";
		$res = mysql_query($SQLquery);
		self::disconnect();
	}

	/**
	 * Checks if the DB exists
	 * @return Boolean True when the DB exists, otherwise false
	 */
	function checkIfDBExists(){
		self::connect();
		if (!mysql_select_db(constant('SQLDB'))){
			self::disconnect();
			return true;
		}else{
			self::disconnect();
			return false;
		}
	}

	function checkIfTableExists($table){
		self::connect();
		if (!mysql_query("SELECT * FROM $table")){
			self::disconnect();
			return true;
		}else{
			self::disconnect();
			return false;
		}
	}

	/**
	 * Connects to the MySQL Server with Settings from the DBCredentials File
	 * @return Object Nothing
	 */
	function connect(){
		mysql_connect(constant('SQLhost'), constant('SQLuser'), constant('SQLpassword'));
	}

	/**
	 * Disconnects from the MySQL Server
	 * @return Object Nothing
	 */
	function disconnect(){
		mysql_close();
	}

	function addEntry($data, $table){
		if (is_array($data)){
			// $this->log->write("Creating User ".$name);
			self::connect();
			mysql_select_db(constant('SQLDB'));
			$SQLquery = "INSERT INTO $table VALUES ('',";
			foreach ($data as $key => $value) {
				$SQLquery .= "'$value',";
			}
			$SQLquery = self::removeLastChar($SQLquery);
			$SQLquery .= ")";
			echo $SQLquery;
			$SQLresult = mysql_query($SQLquery);
			self::disconnect();
			return true;
		}
	}

	private function removeLastChar($string){
		return substr($string, 0, strlen($string) -1);
	}

	function checkForEntry($table, $condition){
		self::connect();
		mysql_select_db(constant('SQLDB'));
		$SQLquery = "SELECT * FROM $table WHERE $condition";
		$SQLresult = mysql_query($SQLquery);
		$SQLrows = mysql_numrows($SQLresult);
		self::disconnect();
		if ($SQLrows > 0){
			return true;
		}else{
			return false;
		}
	}

	function updateEntry($data, $table, $condition){
		self::connect();
		mysql_select_db(constant('SQLDB'));
		$SQLquery = "UPDATE $table SET";
		foreach ($data as $key => $value) {
			$SQLquery .= "$key='$value',";
		}
		$SQLquery = self::removeLastChar($SQLquery);
		$SQLquery .= ") WHERE $condition";
		echo $SQLquery;
		$SQLresult = mysql_query($SQLquery);
		self::disconnect();
	}

	function getEntry($data, $table, $key,$condition, $verbose = false){
		self::connect();
		mysql_select_db(constant('SQLDB'));
		if ($condition != ""){
			$SQLquery = "SELECT * FROM $table WHERE $condition";
		}else{
			$SQLquery = "SELECT * FROM $table";
		}
		$SQLresult = mysql_query($SQLquery);
		$SQLrows = mysql_numrows($SQLresult);
		self::disconnect();
		$SQLcurr = 0;
		if ($SQLrows > 0){
			while ($SQLcurr < $SQLrows) {
				if ($verbose){
					echo mysql_result($SQLresult, $SQLcurr, $key);
				}
				return mysql_result($SQLresult, $SQLcurr, $key);
				$SQLcurr++;
			}
		}else{
			return false;
		}
	}

}