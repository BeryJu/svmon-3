<?php
/**
 * A utility Class for String
 *
 * @package svMon3_Utility
 */
/**
 * Tests wether a String is within another string
 * @param  String $haystack The String to search through
 * @param  String $needle   The String to search for
 * @return Boolean           True if found, otherwise false
 */
function stringContains($haystack,$needle) {  
	if (strpos($haystack,$needle)!==false)  
		return true;  
	else  
		return false;  
}  
/**
 * Tests wether a String starts with another String
 * @param  String $haystack The String to search through
 * @param  String $needle   The String to test for
 * @return Boolean           True if $haystack starts with $needle, otherwise false
 */
function stringStartsWith($haystack,$needle) {  
	if (strpos($haystack,$needle)===0)  
		return true;  
	else  
		return false;  
}  

/**
 * Hashes a String with the Sha512 Hash
 * @param  String $str The String to Hash
 * @return String      The Hashed String
 */
function sha512($str) {
	return hash('sha512', $str.gethostname());;
}
?>