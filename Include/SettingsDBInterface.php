<?php
require_once 'Include/DBInterface.php';
require_once 'Include/String.php';
require_once 'Settings/DBCredentials.php';
class SettingsDBInterface extends DBInterface{

	function __construct(){
		if ($this->checkIfDBExists()){
			parent::createDB();
		}
		if ($this->checkIfTableExists("Settings")){
			parent::createTable("Settings", "name varchar(20) NOT NULL,
				value varchar(128) NOT NULL");
		}
	}

	function addSettings($name, $value){
		$data = array();
		$data['name'] = $name;
		$data['value'] = $value;
		if (parent::checkForEntry("Settings", "name = '$name'")){
			return false;
		}else{
			return parent::addEntry($data, "Settings");
		}
	}

	function getSetting($name){
		$data = array();
		$data['name'] = $name;
		//$data['value'] = "";
		return parent::getEntry($data, "Settings", "value","name = '$name'");
	}

}